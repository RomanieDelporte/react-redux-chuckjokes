const fetchApi = async (
  dispatch,
  loadingAction,
  successAction,
  failedAction,
  url
) => {
  try {
    dispatch({
      type: loadingAction,
    });
    const response = await fetch(url);
    const result = await response.json();
    console.log(result);
    dispatch({ type: successAction, payload: result });
  } catch (error) {
    dispatch({ type: failedAction, error: error.message });
  }
};

export default fetchApi;
