import { Provider } from "react-redux";
import { store, persistor } from "../redux/store/store";
import Container from "../components/container/Container";

function App() {
  return (
    <Provider store={store} persistor={persistor}>
      <Container />
    </Provider>
  );
}

export default App;
