import chuckActions from "../../redux/actions/chuckActions";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

function ChuckJoke() {
  const chuckJoke = useSelector((state) => state.chuckJokeReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(chuckActions.getChuckJoke());
  }, [dispatch]);

  if (chuckJoke.loadingJoke === 1) {
    return <div>Loading...</div>;
  } else if (chuckJoke.failedJoke !== "") {
    return <div>loading</div>;
  } else if (chuckJoke.succesJoke === 1) {
    return <div>{chuckJoke.joke}</div>;
  } else {
    return <></>;
  }
}

export default ChuckJoke;
