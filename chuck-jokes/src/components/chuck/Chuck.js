import chuckActions from "../../redux/actions/chuckActions";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

function Chuck() {
  const chuckCat = useSelector((state) => state.chuckReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(chuckActions.getChuckCategorie());
  }, [dispatch]);

  if (chuckCat.loadingCat === 1) {
    return <div>Loading...</div>;
  } else if (chuckCat.failedCat !== "") {
    return <div>loading</div>;
  } else if (chuckCat.succesCat === 1) {
    return (
      <div className="categorie">
        <p>{chuckCat.cat}</p>
      </div>
    );
  } else {
    return <></>;
  }
}

export default Chuck;
