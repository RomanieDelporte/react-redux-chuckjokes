import "../Container.css";

import Chuck from "../chuck/Chuck";
import ChuckJoke from "../chuck/ChuckJoke";

function Container() {
  return (
    <div className="App">
      <Chuck />
      <ChuckJoke />
    </div>
  );
}

export default Container;
