import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import rootReducer from "../reducers/rootReducer";

import { persistStore, persistReducer, createMigrate } from "redux-persist";
import storage from "redux-persist/lib/storage";

const middleware = [thunk];

if (process.env.NODE_ENV === "development") {
  middleware.push(createLogger());
}

const persistConfig = {
  key: "root",
  storage,
  // data opslaan in storage
  whitelist: ["catReducer"],
  //   version: 2,
  migrate: createMigrate({ debug: false }),
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(...middleware));

const persistor = persistStore(store);

export { store, persistor };
