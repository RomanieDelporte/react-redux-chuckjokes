import { chuckCategorieTypes } from "../actions/chuckActions";
import produce from "immer";

export const initialState = {
  loadingJoke: 0,
  succesJoke: 0,
  joke: "",
  failedJoke: "",
};

const jokeReducer = (previousState = initialState, action) => {
  return produce(previousState, (newState) => {
    switch (action.type) {
      case chuckCategorieTypes.LOADING_JOKE: {
        newState.loadingJoke = 1;
        newState.succesJoke = initialState.succesJoke;
        newState.joke = initialState.payload;
        newState.failedJoke = initialState.failedJoke;
        return newState;
      }
      case chuckCategorieTypes.SUCCES_JOKE: {
        newState.succesJoke = 1;
        newState.joke = action.payload.value;
        newState.loadingJoke = 0;
        return newState;
      }
      case chuckCategorieTypes.FAILED_JOKE: {
        newState.loadingJoke = 0;
        newState.failedJoke = action.error;
        return newState;
      }
      default: {
        return previousState;
      }
    }
  });
};

export default jokeReducer;
