import { chuckCategorieTypes } from "../actions/chuckActions";
import produce from "immer";

export const initialState = {
  loadingCat: 0,
  succesCat: 0,
  cat: "",
  failedCat: "",
  loadingJoke: 0,
  succesJoke: 0,
  joke: "",
  failedJoke: "",
};

const catReducer = (previousState = initialState, action) => {
  return produce(previousState, (newState) => {
    switch (action.type) {
      case chuckCategorieTypes.LOADING_CAT: {
        newState.loadingCat = 1;
        newState.succesCat = initialState.succesCat;
        newState.cat = initialState.cat;
        newState.failedCat = initialState.failedCat;
        return newState;
      }
      case chuckCategorieTypes.SUCCES_CAT: {
        newState.succesCat = 1;
        newState.cat = action.payload;
        newState.loadingCat = 0;
        return newState;
      }
      case chuckCategorieTypes.FAILED_CAT: {
        newState.loadingCat = 0;
        newState.failedCat = action.error;
        return newState;
      }
      default: {
        return previousState;
      }
    }
  });
};

export default catReducer;
