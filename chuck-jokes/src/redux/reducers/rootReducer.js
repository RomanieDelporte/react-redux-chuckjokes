import { combineReducers } from "redux";
import chuckReducer from "./chuckReducer";
import chuckJokeReducer from "./chuckJokeReducer";

const rootReducer = combineReducers({
  chuckReducer: chuckReducer,
  chuckJokeReducer: chuckJokeReducer,
});

export default rootReducer;
