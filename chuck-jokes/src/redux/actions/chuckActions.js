import fetchApi from "../../constants/fetching";

export const chuckCategorieTypes = {
  LOADING_CAT: "LOADING_CAT",
  SUCCES_CAT: "SUCCES_CAT",
  FAILED_CAT: "FAILED_CAT",
  LOADING_JOKE: "LOADING_JOKE",
  SUCCES_JOKE: "SUCCES_JOKE",
  FAILED_JOKE: "FAILED_JOKE",
};

const chuckCatActions = {
  getChuckCategorie: () => {
    return (dispatch) => {
      fetchApi(
        dispatch,
        chuckCategorieTypes.LOADING_CAT,
        chuckCategorieTypes.SUCCES_CAT,
        chuckCategorieTypes.FAILED_CAT,
        "https://api.chucknorris.io/jokes/categories"
      );
    };
  },
  getChuckJoke: () => {
    return (dispatch) => {
      fetchApi(
        dispatch,
        chuckCategorieTypes.LOADING_JOKE,
        chuckCategorieTypes.SUCCES_JOKE,
        chuckCategorieTypes.FAILED_JOKE,
        "https://api.chucknorris.io/jokes/random?category=sport"
      );
    };
  },
};

export default chuckCatActions;
